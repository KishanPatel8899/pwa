import React, { useCallback, useEffect, useState } from "react";
import moment from "moment";
import axios from "axios";
import './History.css'



export const History = () => {

  const [todayprice, settodayprice] = useState(0);
  const [yesterdayprice, setyesterdayprice] = useState(0);
  const [twodaysprice, settwodaysprice] = useState(0);
  const [threedaysprice, setthreedaysprice] = useState(0);
  const [fourdaysprice, setfourdaysprice] = useState(0);

  const getETHPrices = (date) => {
    return axios.get(`https://min-api.cryptocompare.com/data/pricehistorical?fsym=ETH&tsyms=USD&ts=${date}&api_key=ff762a51b2c8c1f295b756c43aa6440d1b85c29fe84f357fd1d88d72038fb9e9`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
  // This function gets the BTC price for a specific timestamp/date. The date is passed in as an argument
  const getBTCPrices = (date) => {
    return axios.get(`https://min-api.cryptocompare.com/data/pricehistorical?fsym=BTC&tsyms=USD&ts=${date}&api_key=ff762a51b2c8c1f295b756c43aa6440d1b85c29fe84f357fd1d88d72038fb9e9`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: "ff762a51b2c8c1f295b756c43aa6440d1b85c29fe84f357fd1d88d72038fb9e9"

      },
    });

  }
  // This function gets the LTC price for a specific timestamp/date. The date is passed in as an argument
  const getLTCPrices = (date) => {
    return axios.get(`https://min-api.cryptocompare.com/data/pricehistorical?fsym=LTC&tsyms=USD&ts=${date}&api_key=ff762a51b2c8c1f295b756c43aa6440d1b85c29fe84f357fd1d88d72038fb9e9`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: "ff762a51b2c8c1f295b756c43aa6440d1b85c29fe84f357fd1d88d72038fb9e9",

      },
    });
  }



  const getTodayPrice = useCallback(() => {
    let today = moment().unix();
    axios.all([getETHPrices(today), getBTCPrices(today), getLTCPrices(today)])
      .then(axios.spread((eth, btc, ltc) => {

        let f = {
          date: moment.unix(today).format("MMMM Do YYYY"),
          eth: eth?.data?.ETH?.USD,
          btc: btc?.data?.BTC?.USD,
          ltc: ltc?.data?.LTC?.USD
        }
        settodayprice(f)
        localStorage.setItem('todayprice', JSON.stringify(f));
      }));
  }, [])



  const getYesterDayPrice = useCallback(
    () => {
      let yesterDayprice = moment().subtract(1, 'days').unix();
      axios.all([getETHPrices(yesterDayprice), getBTCPrices(yesterDayprice), getLTCPrices(yesterDayprice)])
        .then(axios.spread((eth, btc, ltc) => {
          let f = {
            date: moment.unix(yesterDayprice).format("MMMM Do YYYY"),
            eth: eth?.data?.ETH?.USD,
            btc: btc?.data?.BTC?.USD,
            ltc: ltc?.data?.LTC?.USD
          }
          setyesterdayprice(f)
          localStorage.setItem('yesterdayprice', JSON.stringify(f));
        }));
    }, [])
  const getTwoDaysPrice = useCallback(() => {
    let twoDay = moment().subtract(1, 'days').unix();
    axios.all([getETHPrices(twoDay), getBTCPrices(twoDay), getLTCPrices(twoDay)])
      .then(axios.spread((eth, btc, ltc) => {
        let f = {
          date: moment.unix(twoDay).format("MMMM Do YYYY"),
          eth: eth?.data?.ETH?.USD,
          btc: btc?.data?.BTC?.USD,
          ltc: ltc?.data?.LTC?.USD
        }
        settwodaysprice(f)
        localStorage.setItem('twodaysprice', JSON.stringify(twoDay));
      }));
  }, [])

  const getThreeDaysPrice = useCallback(() => {
    let threeDay = moment().subtract(3, 'days').unix();
    axios.all([getETHPrices(threeDay), getBTCPrices(threeDay), getLTCPrices(threeDay)])
      .then(axios.spread((eth, btc, ltc) => {
        let f = {
          date: moment.unix(threeDay).format("MMMM Do YYYY"),
          eth: eth?.data?.ETH?.USD,
          btc: btc?.data?.BTC?.USD,
          ltc: ltc?.data?.LTC?.USD
        }
        setthreedaysprice(f)
        localStorage.setItem('threedaysprice', JSON.stringify(threeDay));
      }));
  }, [])

  const getFourDaysPrice = useCallback(() => {
    let t = moment().subtract(4, 'days').unix();
    axios.all([getETHPrices(t), getBTCPrices(t), getLTCPrices(t)])
      .then(axios.spread((eth, btc, ltc) => {

        let f = {
          date: moment.unix(t).format("MMMM Do YYYY"),
          eth: eth?.data?.ETH?.USD,
          btc: btc?.data?.BTC?.USD,
          ltc: ltc?.data?.LTC?.USD
        }
        setfourdaysprice(f)
        localStorage.setItem('fourdaysprice', JSON.stringify(f));
      }));
  }, [])

  useEffect(() => {
    if (!navigator.onLine) {
      settodayprice({ todayprice: JSON.parse(localStorage.getItem('todayprice')) });
      settwodaysprice({ yesterdayprice: JSON.parse(localStorage.getItem('yesterdayprice')) });
      settwodaysprice({ twodaysprice: JSON.parse(localStorage.getItem('twodaysprice')) });
      setthreedaysprice({ threedaysprice: JSON.parse(localStorage.getItem('threedaysprice')) });
      setfourdaysprice({ fourdaysprice: JSON.parse(localStorage.getItem('fourdaysprice')) });
    }
  }, [])


  useEffect(() => {
    getTodayPrice();
    getYesterDayPrice()
    getThreeDaysPrice()
    getFourDaysPrice();
    getTwoDaysPrice();
  }, [getTodayPrice, getYesterDayPrice, getThreeDaysPrice, getFourDaysPrice, getTwoDaysPrice])

  return <div className="history--section container">
    <h2>History (Past 5 days)</h2>
    <div className="history--section__box">
      <div className="history--section__box__inner">
        <h4>{todayprice?.data}</h4>
        <div className="columns">
          <div className="column">
            <p>1 BTC = ${todayprice.btc}</p>
          </div>
          <div className="column">
            <p>1 ETH = ${todayprice.eth}</p>
          </div>
          <div className="column">
            <p>1 LTC = ${todayprice.ltc}</p>
          </div>
        </div>
      </div>
      <div className="history--section__box__inner">
        <h4>{yesterdayprice?.date}</h4>
        <div className="columns">
          <div className="column">
            <p>1 BTC = ${yesterdayprice.btc}</p>
          </div>
          <div className="column">
            <p>1 ETH = ${yesterdayprice.eth}</p>
          </div>
          <div className="column">
            <p>1 LTC = ${yesterdayprice.ltc}</p>
          </div>
        </div>
      </div>
      <div className="history--section__box__inner">
        <h4>{twodaysprice?.date}</h4>
        <div className="columns">
          <div className="column">
            <p>1 BTC = ${twodaysprice.btc}</p>
          </div>
          <div className="column">
            <p>1 ETH = ${twodaysprice.eth}</p>
          </div>
          <div className="column">
            <p>1 LTC = ${twodaysprice.ltc}</p>
          </div>
        </div>
      </div>
      <div className="history--section__box__inner">
        <h4>{threedaysprice?.date}</h4>
        <div className="columns">
          <div className="column">
            <p>1 BTC = ${threedaysprice.btc}</p>
          </div>
          <div className="column">
            <p>1 ETH = ${threedaysprice.eth}</p>
          </div>
          <div className="column">
            <p>1 LTC = ${threedaysprice.ltc}</p>
          </div>
        </div>
      </div>
      <div className="history--section__box__inner">
        <h4>{fourdaysprice?.date}</h4>
        <div className="columns">
          <div className="column">
            <p>1 BTC = ${fourdaysprice.btc}</p>
          </div>
          <div className="column">
            <p>1 ETH = ${fourdaysprice.eth}</p>
          </div>
          <div className="column">
            <p>1 LTC = ${fourdaysprice.ltc}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
};
