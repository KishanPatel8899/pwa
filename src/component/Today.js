import React, { useState, useEffect, } from "react";
import "./Today.css";
import axios from "axios";
import Pusher from 'pusher-js'

export const Today = () => {
  const btPrice = localStorage.getItem('BTC') ? localStorage.getItem('BTC') : 0;
  const ltcPrice = localStorage.getItem('ETH') ? localStorage.getItem('BTC') : 0;
  const ethPrice = localStorage.getItem('LTC') ? localStorage.getItem('BTC') : 0;

  const [btcprice, setBtprice] = useState(btPrice);
  const [ltcprice, setLtPrice] = useState(ltcPrice);
  const [ethprice, setethPrice] = useState(ethPrice);

  const sendPricePusher = (data) => {
    axios.post('prices/new', {
      prices: data
    })
      .then(response => {
        console.log(response)
      })
      .catch(error => {
        console.log(error)
      })
  }



  useEffect(() => {
    if (!navigator.onLine) {
      setBtprice(localStorage.getItem('BTC'));
      setLtPrice(localStorage.getItem('ETH'));
      setethPrice(localStorage.getItem('LTC'));
    }
    let pusher = new Pusher('c9d9a148a86311755928', {
      cluster: 'ap2',
    });
    const prices = pusher.subscribe('coin-prices');
    setInterval(() => {
      axios.get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,LTC&tsyms=USD&api_key=ff762a51b2c8c1f295b756c43aa6440d1b85c29fe84f357fd1d88d72038fb9e9', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        }
      })
        .then(response => {
          sendPricePusher(response.data)
          localStorage.setItem('BTC', response.data?.BTC?.USD);
          localStorage.setItem('ETH', response.data?.ETH?.USD);
          localStorage.setItem('LTC', response.data?.LTC?.USD);
        })
        .catch(error => {
          console.log(error)
        })
    }, 10000)

    prices.bind('client-msg', (price) => {
      setBtprice(price.prices.BTC?.USD)
      setLtPrice(price.prices.ETH?.USD)
      setethPrice(price.prices.LTC?.USD)
    });
  }, []);

  return (
    <div>
      <div className="today--section container">
        <h2>Current Price</h2>
        <div className="columns today--section__box">
          <div className="column btc--section">
            <h5>${btcprice || 0}</h5>
            <p>1 BTC</p>
          </div>
          <div className="column eth--section">
            <h5>${ethprice || 0}</h5>
            <p>1 ETH</p>
          </div>
          <div className="column ltc--section">
            <h5>${ltcprice || 0}</h5>
            <p>1 LTC</p>
          </div>
        </div>
      </div>
    </div>
  );
};

// class Today extends React.Component {
//   state = {
//     btcprice: 0,
//     ethprice: 0,
//     ltcprice: 0,
//   }

//   sendPricePusher(data) {
//     axios.post('http://localhost:5000/prices/new', {
//       prices: data
//     })
//       .then(response => {
//         console.log(response)
//       })
//       .catch(error => {
//         console.log(error)
//       })
//   }

//   componentDidMount() {
//     // establish a connection to Pusher
//     this.pusher = new Pusher('V08-V4ndCiCtgMovXpLkLN2k4IhO1z1QWe1q0ITIuP0', {
//       cluster: 'ap2',
//       encrypted: true
//     });
//     this.prices = this.pusher.subscribe('coin-prices');
//     setInterval(() => {
//       axios.get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,LTC&tsyms=USD&api_key=ff762a51b2c8c1f295b756c43aa6440d1b85c29fe84f357fd1d88d72038fb9e9', {
//         method: 'GET',
//         headers: {
//           'Content-Type': 'application/json',
//         }
//       })
//         .then(response => {
//           this.sendPricePusher(response.data)
//         })
//         .catch(error => {
//           console.log(error)
//         })
//     }, 10000)

//     console.log(this.prices)
//     // this.prices.bind('prices', price => {
//     //   console.log(price)
//     //   this.setState({ btcprice: price.prices.BTC.USD });
//     //   this.setState({ ethprice: price.prices.ETH.USD });
//     //   this.setState({ ltcprice: price.prices.LTC.USD });
//     // }, this);

//   }


//   render() {
//     return (
//       <div>
//         <div className="today--section container">
//           <h2>Current Price</h2>
//           <div className="columns today--section__box">
//             <div className="column btc--section">
//               <h5>${this.state.btcprice}</h5>
//               <p>1 BTC</p>
//             </div>
//             <div className="column eth--section">
//               <h5>${this.state.ethprice}</h5>
//               <p>1 ETH</p>
//             </div>
//             <div className="column ltc--section">
//               <h5>${this.state.ltcprice}</h5>
//               <p>1 LTC</p>
//             </div>
//           </div>
//         </div>
//       </div>
//     )
//   }
// }

// export { Today }